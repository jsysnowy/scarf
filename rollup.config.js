import dts from 'rollup-plugin-dts';
import externals from 'rollup-plugin-node-externals';
import esbuild from 'rollup-plugin-esbuild';

const name = require('./package.json').main.replace(/\.js$/, '');

const bundle = (config) => ({
    ...config,
    input: 'src/index.ts',
});

export default [
    bundle({
        plugins: [externals(), esbuild()],
        output: [
            {
                file: `${name}.js`,
                format: 'esm',
                sourcemap: true,
            },
        ],
    }),
    bundle({
        plugins: [dts()],
        output: {
            file: `${name}.d.ts`,
        },
    }),
];
