import { Dict } from '@pixi/utils';
import * as PIXI from 'pixi.js';
import { IMediaInstance } from '@pixi/sound';
import * as PIXISpine from 'pixi-spine';
import { IAnimationState, ISkeleton, IAnimationStateData } from 'pixi-spine';

interface ILoadManifest {
    images?: [{
        id: string;
        url: string;
        type: string;
    }];
    json?: [{
        id: string;
        url: string;
        type: string;
    }];
    audio?: [{
        id: string;
        url: string;
        type: string;
    }];
    audiosprites?: [{
        id: string;
        url: string;
        type: string;
    }];
    spines?: [{
        id: string;
        url: string;
        type: string;
        textures: string[];
        jsonPath: string;
    }];
    warnings?: any[];
    assetCount?: number;
}

/**
 * Stores the different possible display scale modes for a container:
 * @export
 * @enum {number}
 */
declare enum EDisplayScaleMode {
    NONE = 0,
    FIT = 1,
    FILL = 2,
    STRETCH = 3
}
declare class ScaleManager {
    /**
     * Stores the current available space:
     * @private
     * @type {{ width: number, height: number }}
     * @memberof ScaleManager
     */
    private _currentAvailableSpace;
    /**
     * Stores the current x, y, min and max ratios of canvas size to native size:
     * @private
     * @type {{ x: number; y: number; min: number; max: number }}
     * @memberof ScaleManager
     */
    private _currentRatio;
    /**
     * Stores the settings:
     * @private
     * @type {IGameSettings}
     * @memberof ScaleManager
     */
    private _settings;
    /**
     * Creates an instance of ScaleManager.
     * @param {IGameSettings} settings
     * @memberof ScaleManager
     */
    constructor(settings: IGameSettings);
    /**
     * Updates the renderer dimensions and scales all AspectScaled groups accordingly.
     * @param {PIXI.Renderer} renderer
     * @param {Container} stage
     * @memberof ScaleManager
     */
    updateScale(renderer: PIXI.Renderer, stage: Container): void;
    /**
     *
     * @private
     * @return {*}  {{ x: number, y: number }}
     * @memberof ScaleManager
     */
    private _getAvailableSpace;
    /**
     * Processes aspect scale for a container:
     * @private
     * @memberof ScaleManager
     */
    private _processAspectScale;
    /**
     * Applies a fit scalemode to a container:
     * @private
     * @param {Container} groupIn
     * @memberof ScaleManager
     */
    private _applyFitScaleMode;
    /**
     * Applies a fit scalemode to a container:
     * @private
     * @param {Container} groupIn
     * @memberof ScaleManager
     */
    private _applyFillScaleMode;
}

declare class Container extends PIXI.Container {
    /**
     * Stores a potential update function which will be called if it exists:
     * @memberof Container
     */
    update?: (dt: number) => void;
    /**
     * Stores if this container can be paused, or not.  This affects the container, and all children recursively.
     * @type {boolean}
     * @memberof Container
     */
    canBePaused: boolean;
    /**
     * Stores the display scale mode of this container:
     * @private
     * @type {EDisplayScaleMode}
     * @memberof Container
     */
    private _displayScaleMode;
    /**
     * Stores the scale anchor:
     * @private
     * @type {{ x: number, y: number }}
     * @memberof Container
     */
    private _scaleAnchor;
    /**
     * Sets the display scale mode:
     * @memberof Container
     */
    set displayScaleMode(v: EDisplayScaleMode);
    /**
     * Returns the scale mode:
     * @type {EDisplayScaleMode}
     * @memberof Container
     * @remark This doesnt work
     */
    get displayScaleMode(): EDisplayScaleMode;
    /**
     * Returns the scale anchor:
     * @readonly
     * @type {{ x: number, y: number }}
     * @memberof Container
     */
    get scaleAnchor(): {
        x: number;
        y: number;
    };
    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    get scaleX(): number;
    /**
     * Sets scaleX of this container.
     * @memberof Container
     */
    set scaleX(x: number);
    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    get scaleY(): number;
    /**
     * Sets scaleY of this container.
     * @memberof Container
     */
    set scaleY(y: number);
    /**
     * Creates an instance of Container.
     * @param {number} [x]
     * @param {number} [y]
     * @param {string} [name]
     * @memberof Container
     */
    constructor(x?: number, y?: number, name?: string);
    /**
     * Overwrites destroy with custom things:
     * @memberof Container
     */
    destroy(): void;
}

declare class Scene extends Container {
    /**
     * Preloads assets needed for the scene:
     * @return {*}  {Promise<void>}
     * @memberof Scene
     */
    preload(preloadManifest?: ILoadManifest): Promise<void>;
    /**
     * Creates the scene:
     * @memberof Scene
     */
    create(): void;
}

/**
 * Stores an array of sounds which are grouped and controlled together.
 * @export
 * @class SoundChannel
 */
declare class SoundChannel {
    /**
     * Stores the default settings for the soundchannel for if bespoke audio settings are not provided:
     * @private
     * @memberof SoundManager
     */
    private _defaultSettings;
    /**
     * Stores the name of this channel:
     * @private
     * @type {string}
     * @memberof SoundChannel
     */
    private _name;
    /**
     * Stores the volume of this channel:
     * @private
     * @type {number}
     * @memberof SoundChannel
     */
    private _volume;
    /**
     * Stores if this channel is muted or not:
     * @private
     * @type {boolean}
     * @memberof SoundChannel
     */
    private _muted;
    /**
     * Stores if the channel is paused or not:
     * @private
     * @type {boolean}
     * @memberof SoundChannel
     */
    private _paused;
    /**
     * Stores all the sounds in this channel:
     * @private
     * @type {Sound[]}
     * @memberof SoundChannel
     */
    private _sounds;
    /**
     * Returns volume of this channel:
     * @readonly
     * @type {number}
     * @memberof SoundChannel
     */
    get volume(): number;
    /**
     * Creates an instance of SoundChannel.
     * @memberof SoundChannel
     */
    constructor(name: string);
    /**
     * Adds a sound to this channel:
     * @param {Sound} s
     * @memberof SoundChannel
     */
    addSound(s: Sound): void;
    /**
     * Remove a sound from this channel:
     * @param {Sound} s
     * @memberof SoundChannel
     */
    removeSound(s: Sound): void;
    /**
     * Sets volume of this channel:
     * @param {number} v
     * @memberof SoundChannel
     */
    setVolume(v: number): void;
    /**
     * Mute this channel:
     * @memberof SoundChannel
     */
    mute(): void;
    /**
     * Unmute this channel:
     * @memberof SoundChannel
     */
    unmute(): void;
    /**
     * Pause this channel:
     * @memberof SoundChannel
     */
    pause(): void;
    /**
     * Resume this channel:
     * @memberof SoundChannel
     */
    resume(): void;
}

declare class Sound {
    /**
     * Stores the audiosprite ID - if this sound is audiosprite instance:
     * @private
     * @type {string}
     * @memberof Sound
     */
    private _audioSpriteID?;
    /**
     * Stores the soundID of this sound:
     * @private
     * @type {string}
     * @memberof Sound
     */
    private _soundID;
    /**
     * Stores the channel reference:
     * @private
     * @type {SoundChannel}
     * @memberof Sound
     */
    private _channelRef;
    /**
     * Stores the pixi sound.
     * @private
     * @type {PIXISound}
     * @memberof Sound
     */
    private _soundRef?;
    /**
     * Stores the media instance:
     * @private
     * @type {IMediaInstance}
     * @memberof Sound
     */
    private _mediaInstance?;
    /**
     * Stores the sounds volume:
     * @private
     * @type {number}
     * @memberof Sound
     */
    private _volume;
    /**
     * Stores if the sound is muted or not:
     * @private
     * @type {boolean}
     * @memberof Sound
     */
    private _muted;
    /**
     * Stores if the sound is paused or not:
     * @private
     * @type {boolean}
     * @memberof Sound
     */
    private _paused;
    /**
     * Stores current progress:
     * @private
     * @type {number}
     * @memberof Sound
     */
    private _curProg;
    /**
     * Stores duration of the sound:
     * @private
     * @type {number}
     * @memberof Sound
     */
    private _soundDuration;
    /**
     * Returns the soundID of this sound:
     * @readonly
     * @type {string}
     * @memberof Sound
     */
    get soundID(): string;
    /**
     * Returns the media instance:
     * @readonly
     * @type {IMediaInstance}
     * @memberof Sound
     */
    get mediaInstance(): IMediaInstance;
    /**
     * Sets the channel this sound is playing in:
     * @memberof Sound
     */
    set channel(c: SoundChannel);
    /**
     * Gets the sound channel this sound is playing in
     * @type {SoundChannel}
     * @memberof Sound
     */
    get channel(): SoundChannel;
    /**
     * Creates an instance of Sound.
     * @param {string} id
     * @memberof Sound
     */
    constructor(id: string, channel: SoundChannel);
    /**
     * Tells this sound instance to play:
     * @param {boolean} _loop
     * @return {*}  {Promise<void>}
     * @memberof Sound
     */
    play(_loop: boolean): Promise<void>;
    /**
     * Tells this sound instance to stop:
     * @memberof Sound
     */
    stop(): void;
    /**
     * Sets volume of this media instance:
     * @param {number} v
     * @memberof Sound
     */
    setVolume(v: number): void;
    /**
     * Mutes this mediaInstance:
     * @memberof Sound
     */
    mute(): void;
    /**
     * Unmutes this media instance:
     * @memberof Sound
     */
    unmute(): void;
    /**
     * Pauses this sound:
     * @memberof Sound
     */
    pause(): void;
    /**
     * Resumes this sound:
     * @memberof Sound
     */
    resume(): void;
}

declare class Wrapper {
    /**
     * Initializes the wrapper:
     * @return {*}  {Promise<void>}
     * @memberof Wrapper
     */
    init(): Promise<void>;
    /**
     * Requests config file from integration:
     * @return {*}  {Promise<void>}
     * @memberof Wrapper
     */
    requestConfig(): Promise<{}>;
    /**
     * Requests ticket file from integration:
     * @return {*}  {Promise<void>}
     * @memberof Wrapper
     */
    requestTicket(): Promise<{}>;
}

/**
 * Check out Spine API for more info:
 * @link {https://github.com/pixijs/spine}
 * @export
 * @class Spine
 * @extends {Container}
 */
declare class Spine extends Container {
    /**
     * Stores if this spine is paused or not:
     * @private
     * @type {boolean}
     * @memberof Spine
     */
    private _paused;
    /**
     * Stores functions tied to onCompletes of this spine anim.
     * @private
     * @memberof Spine
     */
    private _onAnimationCompletePromiseMap;
    /**
     * Stores functions tied to onCompletes of this spine anim.
     * @private
     * @type {*}
     * @memberof Spine
     */
    private _eventFunctions;
    /**
     * Stores the spine object:
     * @private
     * @type {( PIXISpine.Spine;}
     * @memberof Spine
     */
    private _spineObject;
    /**
     * Returns if this spine is paused or not:
     * @readonly
     * @type {boolean}
     * @memberof Spine
     */
    get isPaused(): boolean;
    /**
     * Return the spine object for this instance of core.Spine.
     * @readonly
     * @type {PIXISpine.Spine}
     * @memberof Spine
     */
    get spineObject(): PIXISpine.Spine;
    /**
     * Returns the spine state:
     * @readonly
     * @type {IAnimationState}
     * @memberof Spine
     */
    get state(): IAnimationState;
    /**
     * Returns the spine state:
     * @readonly
     * @type {ISkeleton}
     * @memberof Spine
     */
    get skeleton(): ISkeleton;
    /**
     * Returns the state data for this spine:
     * @readonly
     * @type {IAnimationStateData}
     * @memberof Spine
     */
    get stateData(): IAnimationStateData;
    /**
     * Creates an instance of Spine.
     * @param {string} id
     * @memberof Spine
     */
    constructor(id: string | {
        id: string;
        animations: any;
        skins: any;
        events: any;
        replacableSlots: any;
    });
    /**
     * Removes all event listeners for this Spine.
     * @memberof Spine
     */
    clearAllListeners(): void;
    /**
     * Set onAnimationComplete function!
     * @param {string} animName
     * @param {()=>any} onCompleteFunc
     * @param {boolean} once
     * @memberof Spine
     */
    onAnimationComplete(animName: string, onCompleteFunc: () => void, once?: boolean): void;
    /**
     * Triggers when a certain event calls.
     * @param {string} eventName
     * @param {()=>any} callback
     * @param {boolean} [once=false]
     * @memberof Spine
     */
    onEvent(eventName: string, callback: () => void, once?: boolean): void;
    /**
     * Called every frame, this makes sure our spine object is tied into the core game loop.
     * @private
     * @param {number} dT difference in time
     * @memberof Spine
     */
    update: (dT: number) => void;
    /**
     * Pauses this spine:
     * @memberof Spine
     */
    pause(): void;
    /**
     * Unpauses this spine:
     * @memberof Spine
     */
    unpause(): void;
    /**
     * Super also removes the updateFunc.
     * @memberof Spine
     */
    destroy(): void;
    /**
     * Attaches spine listeners onto this spineObject.
     * @private
     * @memberof Spine
     */
    private _addSpineListeners;
}

declare class Sprite extends PIXI.Sprite {
    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    get scaleX(): number;
    /**
     * Sets scaleX of this container.
     * @memberof Container
     */
    set scaleX(x: number);
    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    get scaleY(): number;
    /**
     * Creates an instance of Sprite.
     * @param {number} x
     * @param {number} y
     * @param {string} spriteID
     * @memberof Sprite
     */
    constructor(x: number, y: number, spriteID: string);
    /**
     * Handles this object being destroyed:
     * @param {(boolean | PIXI.IDestroyOptions)} [options]
     * @memberof Sprite
     */
    destroy(options?: boolean | PIXI.IDestroyOptions): void;
}

/**
 * Exports objects under the SCARF namespace:
 */

type scarf_Sprite = Sprite;
declare const scarf_Sprite: typeof Sprite;
type scarf_Wrapper = Wrapper;
declare const scarf_Wrapper: typeof Wrapper;
type scarf_Sound = Sound;
declare const scarf_Sound: typeof Sound;
type scarf_Spine = Spine;
declare const scarf_Spine: typeof Spine;
type scarf_Container = Container;
declare const scarf_Container: typeof Container;
declare namespace scarf {
  export {
    scarf_Sprite as Sprite,
    scarf_Wrapper as Wrapper,
    scarf_Sound as Sound,
    scarf_Spine as Spine,
    scarf_Container as Container,
  };
}

interface IGameDisplaySettings {
    nativeWidth: number;
    nativeHeight: number;
    canvasWidth: number | 'dynamic';
    canvasHeight: number | 'dynamic';
    orientation: 'portrait' | 'landscape' | 'dynamic';
}
interface IGameSettings {
    loadManifest: ILoadManifest;
    loader: Scene;
    wrapper: Wrapper;
    display: IGameDisplaySettings;
    globalTimeline: gsap.core.Timeline;
}
declare const defaultGameSettings: IGameSettings;

/**
 * Controls playing of sounds in a game:
 *
 * Required features:
 * Play
 * Loop
 * Stop
 * Volume
 * NumberOfSoundInstances ( Stop sound overlapping )
 * Channels
 * BGM/SFX split
 * Pause
 * Mute
 *
 * @export
 * @class SoundManager
 */
declare class SoundManager {
    /**
     * Stores the default settings for the sound manager for if bespoke audio settings are not provided:
     * @private
     * @memberof SoundManager
     */
    private _defaultSettings;
    /**
     * Stores channels
     * @private
     * @type {Scarf.Sound[]}
     * @memberof SoundManager
     */
    private _channels;
    /**
     * Creates an instance of SoundManager.
     * @memberof SoundManager
     */
    constructor();
    /**
     * Initializes the Scarf sound manager:
     * @memberof SoundManager
     */
    init(): void;
    /**
     * Plays a track - pass in ID (string) of track you want to play along with channel name.  If passed in channel doesn't exist, it will create it using default settings.
     * Optinal settings param allows extra configuration, falling back to default audio settings.

     * @return {*}  {Promise<void>}
     * @memberof SoundManager
     */
    play(id: string, channel?: string, _settings?: {
        allowMultiple?: boolean;
        overwrite?: boolean;
        loop?: boolean;
    }): Promise<Sound>;
    /**
     * Tells the soundmanager to stop a certain sound.
     * @memberof SoundManager
     */
    stop(): void;
    /**
     * Tells sound manafer ti stop all sounds with a certain name:
     * @memberof SoundManager
     */
    stopAllSoundsByName(): void;
    /**
     * Returns volume of soundMnager, or a specific channel.
     * @param {string} [channel]
     * @return {*}  {number}
     * @memberof SoundManager
     */
    getVolume(channel?: string): number;
    /**
     * Sets volume of the soundmanager, or a specific channel.
     * @param {number} volume
     * @param {string} [channel]
     * @memberof SoundManager
     */
    setVolume(volume: number, channel?: string): void;
    /**
     * Mutes all sounds, or a specific channel.
     * @memberof SoundManager
     */
    mute(channel?: string): void;
    /**
     * Unmutes all sounds, or a specific channel.
     * @memberof SoundManager
     */
    unmute(channel?: string): void;
    /**
     * Pauses all sounds, or a specific channel.
     * @memberof SoundManager
     */
    pause(channel?: string): void;
    /**
     * Unpauses all sounds, or a specific channel.
     * @memberof SoundManager
     */
    resume(channel?: string): void;
}
/**
 * Exported instance of soundmanager accessable directly through global scope.
 * */
declare let sound: SoundManager;

declare class ScarfGame {
    /**
     * Stores the latest instance of the ScarfGame as a static variable to be exposed globally:
     * @private
     * @static
     * @type {ScarfGame}
     * @memberof ScarfGame
     */
    private static _instance;
    /**
     * Stores the main pixi.js renderer:
     * @private
     * @type {Renderer}
     * @memberof ScarfGame
     */
    private _renderer;
    /**
     * Stores the main game stage - the container which is rendered:
     * @private
     * @type {Container}
     * @memberof ScarfGame
     */
    private _stage;
    /**
     * Stores the settings:
     * @private
     * @type {*}
     * @memberof ScarfGame
     */
    private _settings;
    /**
     * Stores reference to the wrapper:
     * @private
     * @type {Wrapper}
     * @memberof ScarfGame
     */
    private _wrapper;
    /**
     * Stores a scalemanager instance to handle canvas size:
     * @private
     * @type {ScaleManager}
     * @memberof ScarfGame
     */
    private _scale;
    /**
     * Stores the PauseManager;
     * @private
     * @type {PauseManager}
     * @memberof ScarfGame
     */
    private _pause;
    /**
     * Stores a soundmanager instance to handle audio:
     * @private
     * @type {SoundManager}
     * @memberof ScarfGame
     */
    private _sound;
    /**
     * Stores if updates are currently paused or not:
     * @private
     * @type {boolean}
     * @memberof ScarfGame
     */
    private _updatesPaused;
    /**
     * Returns the current instance of scarfgame:
     * @readonly
     * @static
     * @type {ScarfGame}
     * @memberof ScarfGame
     */
    static get instance(): ScarfGame;
    /**
     * Returns reference to the main game stage:
     * @readonly
     * @type {Container}
     * @memberof ScarfGame
     */
    get stage(): Container;
    /**
     * Returns instance of the wrapper:
     * @readonly
     * @type {Wrapper}
     * @memberof ScarfGame
     */
    get wrapper(): Wrapper;
    /**
     * Returns instance of the scalemanager:
     * @readonly
     * @type {ScaleManager}
     * @memberof ScarfGame
     */
    get scale(): ScaleManager;
    /**
     * Returns instance of the soundmanager:
     * @readonly
     * @type {SoundManager}
     * @memberof ScarfGame
     */
    get sound(): SoundManager;
    /**
     * Returns the loader:
     * @readonly
     * @type {Dict<LoaderResource>}
     * @memberof ScarfGame
     */
    get loader(): Dict<PIXI.LoaderResource>;
    /**
     * Returns the game settings:
     * @readonly
     * @type {IGameSettings}
     * @memberof ScarfGame
     */
    get settings(): IGameSettings;
    /**
     * Creates an instance of ScarfGame.
     * @memberof ScarfGame
     */
    constructor(settings: any);
    /**
     * Initializes ScarfGame core:
     * @return {*}  {Promise<void>}
     * @memberof ScarfGame
     */
    init(): Promise<void>;
    /**
     * Pauses the game:
     * @memberof ScarfGame
     */
    pauseGame(): void;
    /**
     * Resumes the game:
     * @memberof ScarfGame
     */
    resumeGame(): void;
    /**
     * Main gameloop of ScarfGame:
     * @private
     * @param {number} _time
     * @param {number} _dt
     * @param {number} _frame
     * @memberof ScarfGame
     */
    private _gameLoop;
    /**
     * Updates through and "update" funcion if it exists:
     * @private
     * @param {*} g
     * @param {number} dt
     * @memberof ScarfGame
     */
    private _updateGroups;
}
declare let game: ScarfGame;

type index_ScaleManager = ScaleManager;
declare const index_ScaleManager: typeof ScaleManager;
type index_SoundManager = SoundManager;
declare const index_SoundManager: typeof SoundManager;
declare namespace index {
  export {
    index_ScaleManager as ScaleManager,
    index_SoundManager as SoundManager,
  };
}

/**
 * Returns half of the native width of the game, useful for positioning items on screen.
 * @returns number
 */
declare const centerX: () => number;
/**
 * Returns half of the native height of the game, useful for positioning items on screen.
 * @returns number
 */
declare const centerY: () => number;

declare class Idle {
    /**
     * Stores all creates idles
     * @private
     * @static
     * @type {Map<string, Idle>}
     * @memberof Idle
     */
    private static _idleStorageMap;
    /**
     * Stores this idles config:
     * @private
     * @type {IIdle}
     * @memberof Idle
     */
    private _config;
    /**
     * Stores an array of all idleFunctions handling idles starting to trigger on delay resolve.
     * @private
     * @type {((...params: any[]) => void)[]}
     * @memberof Idle
     */
    private _idleStartFunctions;
    /**
     * Stores an array of all idleFunctions handling idles starting interrupting.
     * @private
     * @type {((...params: any[]) => void)[]}
     * @memberof Idle
     */
    private _idleInterruptFunctions;
    /**
     * Creates an instance of Idle.
     * @param {string} name
     * @param {number} delay
     * @memberof Idle
     */
    private constructor();
    /**
     * Creates a new idle using the factory. Returns false if the idle already existed.
     * @static
     * @param {string} name
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static create(name: string): boolean;
    /**
     * Gets an idle from the idleStorageMap based on passed in name.
     * @static
     * @param {string} name
     * @return {*}  {Idle}
     * @memberof Idle
     */
    static get(name: string): Idle | undefined;
    /**
     * Destroys idle with passed in name if it exists. Returns false if the idle didn't exist.
     * @static
     * @param {string} name
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static destroy(name: string): boolean;
    /**
     * Starts idle with the passed in name if it exists. Returns false if the idle didn't exist.  Optional delay override for changing the delay on this start.
     * @static
     * @param {string} name
     * @param {number} [delayOverride=-1]
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static start(name: string, delayOverride?: number): boolean;
    /**
     * Interrupts idle with the passed in name if it exists. Returns false if the idle didn't exist.
     * @static
     * @param {string} name
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static interrupt(name: string): boolean;
    /**
     * All idle start functions call.
     * @memberof Idle
     */
    start(): void;
    /**
     * All idle interrupt functions call.
     * @memberof Idle
     */
    interrupt(): void;
    /**
     * Destroys this idle. Cleanup listeners.
     * @memberof Idle
     */
    destroy(): void;
}

export { EDisplayScaleMode, Idle, index as Managers, scarf as Scarf, ScarfGame, SoundManager, centerX, centerY, defaultGameSettings, game, sound };
