import gsap from 'gsap';
import * as PIXI from 'pixi.js';
import { Texture, Text, Graphics } from 'pixi.js';
import * as PIXISpine from 'pixi-spine';
import { TextureAtlas } from 'pixi-spine';
import '@pixi/sound';

const GenerateSpineAtlas = (atlasData) => {
  const allTex = {};
  atlasData.forEach((texture) => {
    allTex[texture] = Texture.from(texture);
  });
  let spineAtlas = new TextureAtlas();
  spineAtlas.addTextureHash(allTex, true);
  const atlas = new TextureAtlas();
  atlas.addTextureHash(allTex, true);
  return atlas;
};

/**
 * @author Rowan
 * @copyright Rowan 2022
 * @license ROW_LIC
 * @export
 * @class Loader
 */
class Loader {
  static _loaderInstance;
  static _loaderScene;
  static get resources() {
    return this._loaderInstance.resources;
  }
  static set loaderScene(s) {
    this._loaderScene = s;
  }
  static get loaderScene() {
    return this._loaderScene;
  }
  static async load(manifest) {
    if (this._loaderInstance == null) {
      this._loaderInstance = new PIXI.Loader();
    }
    let total = manifest.assetCount != null ? manifest.assetCount : 0;
    let c = 0;
    this._loaderInstance.onProgress.add((_l, _r) => {
      c++;
      const progress = c / total;
      if (this._loaderScene != null) {
        this._loaderScene.setLoadingPercentage(progress);
      }
    });
    if (manifest.json != null) {
      manifest.json.forEach((element) => {
        if (element.type == "spritesheet") {
          this._loaderInstance.add(element.id, element.url);
        } else if (element.type == "spine") ; else {
          this._loaderInstance.add(element.id, element.url);
        }
      });
    }
    if (manifest.images != null) {
      manifest.images.forEach((element) => {
        this._loaderInstance.add(element.id, element.url);
      });
    }
    if (manifest.audio != null) {
      manifest.audio.forEach((element) => {
        this._loaderInstance.add(element.id, element.url);
      });
    }
    if (manifest.audiosprites != null) {
      manifest.audiosprites.forEach((element) => {
        this._loaderInstance.add(element.id, element.url);
      });
    }
    if (manifest.warnings != null) {
      if (manifest.warnings != null && manifest.warnings.length != 0) {
        console.group("Warnings");
        manifest.warnings.forEach((err) => {
          console.warn(`[Scarf Warning] Missing image: ${err[1]} from spine ${err[3]}`);
        });
        console.groupEnd();
      }
    }
    await new Promise((resolve, reject) => {
      this._loaderInstance.onError.once(() => {
        reject("Primary load has failed.  This load handled images/spritesheets/json/audio/audiosprite.");
      });
      this._loaderInstance.onComplete.once(() => {
        if (manifest.spines != null) {
          manifest.spines.forEach((element) => {
            if (element.type == "atlas") {
              const atlas = GenerateSpineAtlas(element.textures);
              this._loaderInstance.add(element.id, element.jsonPath, { metadata: { spineAtlas: atlas } });
            }
          });
        }
        resolve();
      });
      this._loaderInstance.load();
    });
    this._loaderInstance.onError.detachAll();
    this._loaderInstance.onComplete.detachAll();
    await new Promise((resolve, reject) => {
      this._loaderInstance.onError.once(() => {
        reject("Secondary load has failed. This load handled spines.");
      });
      this._loaderInstance.onComplete.once(() => {
        manifest.audiosprites?.forEach((audiosprite) => {
          const audiospriteJsonData = Loader.resources[audiosprite.id + "_data"]?.data?.sprite;
          if (audiospriteJsonData != null) {
            const audioSpritesOutputData = {};
            Object.keys(audiospriteJsonData).forEach((key) => {
              audioSpritesOutputData[key] = {
                start: +audiospriteJsonData[key][0] / 1e3,
                end: +(audiospriteJsonData[key][0] + audiospriteJsonData[key][1]) / 1e3
              };
            });
            Loader.resources[audiosprite.id].sound?.addSprites(audioSpritesOutputData);
          }
        });
        resolve();
      });
      this._loaderInstance.load();
    });
  }
}

var EDisplayScaleMode = /* @__PURE__ */ ((EDisplayScaleMode2) => {
  EDisplayScaleMode2[EDisplayScaleMode2["NONE"] = 0] = "NONE";
  EDisplayScaleMode2[EDisplayScaleMode2["FIT"] = 1] = "FIT";
  EDisplayScaleMode2[EDisplayScaleMode2["FILL"] = 2] = "FILL";
  EDisplayScaleMode2[EDisplayScaleMode2["STRETCH"] = 3] = "STRETCH";
  return EDisplayScaleMode2;
})(EDisplayScaleMode || {});
class ScaleManager {
  _currentAvailableSpace = { width: 1, height: 1 };
  _currentRatio = { x: 1, y: 1, min: 1, max: 1 };
  _settings;
  constructor(settings) {
    this._settings = settings;
  }
  updateScale(renderer, stage) {
    this._currentAvailableSpace = this._getAvailableSpace();
    this._currentRatio.x = this._currentAvailableSpace.width / this._settings.display.nativeWidth;
    this._currentRatio.y = this._currentAvailableSpace.height / this._settings.display.nativeHeight;
    this._currentRatio.min = Math.min(this._currentRatio.x, this._currentRatio.y);
    this._currentRatio.max = Math.max(this._currentRatio.x, this._currentRatio.y);
    renderer.resize(this._currentAvailableSpace.width, this._currentAvailableSpace.height);
    this._processAspectScale(stage, false);
  }
  _getAvailableSpace() {
    return { width: window.innerWidth, height: window.innerHeight };
  }
  _processAspectScale(groupIn, found) {
    if (groupIn.displayScaleMode != null && groupIn.displayScaleMode != 0 /* NONE */) {
      if (found) {
        console.warn(`[ScarfWarning] Found nested AspectScaledContainer in ${groupIn.name} which was ignored.`);
      } else {
        if (groupIn.displayScaleMode == 1 /* FIT */) {
          found = true;
          this._applyFitScaleMode(groupIn);
        } else if (groupIn.displayScaleMode == 2 /* FILL */) {
          found = true;
          this._applyFillScaleMode(groupIn);
        }
      }
    }
    groupIn.children.forEach((childGroup) => {
      this._processAspectScale(childGroup, found);
    });
  }
  _applyFitScaleMode(groupIn) {
    const topOffset = -((this._settings.display.nativeHeight * this._currentRatio.min - this._currentAvailableSpace.height) * groupIn.scaleAnchor.y);
    const leftOffset = -((this._settings.display.nativeWidth * this._currentRatio.min - this._currentAvailableSpace.width) * groupIn.scaleAnchor.x);
    groupIn.position.set(leftOffset, topOffset);
    groupIn.scale.set(this._currentRatio.min);
  }
  _applyFillScaleMode(groupIn) {
    const topOffset = -((this._settings.display.nativeHeight * this._currentRatio.min - this._currentAvailableSpace.height) * groupIn.scaleAnchor.y);
    const leftOffset = -((this._settings.display.nativeWidth * this._currentRatio.min - this._currentAvailableSpace.width) * groupIn.scaleAnchor.x);
    groupIn.position.set(leftOffset, topOffset);
    groupIn.scale.set(this._currentRatio.min);
  }
}

class Container extends PIXI.Container {
  update;
  canBePaused = true;
  _displayScaleMode = EDisplayScaleMode.NONE;
  _scaleAnchor = { x: 0.5, y: 0.5 };
  set displayScaleMode(v) {
    this._displayScaleMode = v;
  }
  get displayScaleMode() {
    return this._displayScaleMode;
  }
  get scaleAnchor() {
    return this._scaleAnchor;
  }
  get scaleX() {
    return this.scale.x;
  }
  set scaleX(x) {
    this.scale.x = x;
  }
  get scaleY() {
    return this.scale.y;
  }
  set scaleY(y) {
    this.scale.y = y;
  }
  constructor(x = 0, y = 0, name) {
    super();
    this.x = x;
    this.y = y;
    this.name = name || "[Unnamed Container]";
  }
  destroy() {
    gsap.killTweensOf(this);
    super.destroy({ children: true });
  }
}

class PauseManager {
  _paused = false;
  _setUpdatePause;
  _globalTimeline;
  _pausedTweens = [];
  _pausedTimelines = [];
  _pausedDelayedCalls = [];
  constructor(timeline, setPauseState) {
    this._globalTimeline = timeline;
    this._setUpdatePause = setPauseState;
  }
  pause() {
    this._paused = true;
    return this._paused;
  }
  resume() {
    this._paused = false;
    this.resumeAllTweens();
    this.resumeAllUpdates();
    return this._paused;
  }
  update() {
    if (this._paused) {
      this.pauseAllTweens();
      this.pauseAllUpdates();
    }
  }
  pauseAllTweens() {
    const allTweens = this._globalTimeline.getChildren(false, true, true);
    allTweens.forEach((tween) => {
      if (tween instanceof gsap.core.Tween) {
        if (!tween.paused()) {
          tween.timeScale(0);
          tween.paused(true);
          this._pausedTweens.push(tween);
        }
      } else {
        if (!tween.paused()) {
          tween.timeScale(0);
          tween.paused(true);
          this._pausedTimelines.push(tween);
        }
      }
    });
  }
  resumeAllTweens() {
    this._pausedTweens.forEach((tween) => {
      tween.timeScale(1);
      tween.resume();
    });
    this._pausedTimelines.forEach((tween) => {
      tween.timeScale(1);
      tween.resume();
    });
    this._pausedTweens = [];
    this._pausedTimelines = [];
  }
  pauseAllUpdates() {
    this._setUpdatePause(true);
  }
  resumeAllUpdates() {
    this._setUpdatePause(false);
  }
  pauseAllSounds() {
  }
}

class SoundChannel {
  _defaultSettings = {
    allowMultiple: false,
    overwrite: true,
    loop: false
  };
  _name;
  _volume = 1;
  _muted = false;
  _paused = false;
  _sounds = [];
  get volume() {
    return this._volume;
  }
  constructor(name) {
    this._name = name;
  }
  addSound(s) {
    if (s.channel != null) {
      s.channel.removeSound(s);
    }
    this._sounds.push(s);
  }
  removeSound(s) {
    if (this._sounds.includes(s)) {
      this._sounds.splice(this._sounds.indexOf(s), 1);
    }
  }
  setVolume(v) {
    this._volume = Math.min(Math.max(v, 0), 1);
    this._sounds.forEach((sound) => {
      sound.setVolume(this._volume);
    });
  }
  mute() {
    this._muted = true;
    this._sounds.forEach((sound) => {
      sound.mute();
    });
  }
  unmute() {
    this._muted = false;
    this._sounds.forEach((sound) => {
      sound.unmute();
    });
  }
  pause() {
    this._paused = true;
    this._sounds.forEach((sound) => {
      sound.pause();
    });
  }
  resume() {
    this._paused = false;
    this._sounds.forEach((sound) => {
      sound.resume();
    });
  }
}

class Sound {
  _audioSpriteID;
  _soundID;
  _channelRef;
  _soundRef;
  _mediaInstance;
  _volume = 1;
  _muted = false;
  _paused = false;
  _curProg = 0;
  _soundDuration = 0;
  get soundID() {
    return this._soundID;
  }
  get mediaInstance() {
    return this._mediaInstance;
  }
  set channel(c) {
    this._channelRef = c;
  }
  get channel() {
    return this._channelRef;
  }
  constructor(id, channel) {
    if (id.includes("/")) {
      this._audioSpriteID = id.split("/")[0];
      this._soundID = id.split("/")[1];
    } else {
      this._soundID = id;
    }
    this._channelRef = channel;
  }
  async play(_loop) {
    this._soundRef = Loader.resources[this._audioSpriteID != null ? this._audioSpriteID : this._soundID].sound;
    if (this._audioSpriteID == null) {
      this._soundDuration = this._soundRef.duration;
    } else {
      this._soundDuration = this._soundRef.sprites[this._soundID].duration;
    }
    if (this._soundRef != null) {
      this._soundRef.loop = _loop;
      const instance = this._audioSpriteID != null ? this._soundRef.play({ sprite: this._soundID, start: this._curProg * this._soundDuration }) : this._soundRef.play({ start: this._curProg * this._soundDuration });
      if (instance instanceof Promise) {
        this._mediaInstance = await instance;
      } else {
        this._mediaInstance = instance;
      }
    }
    this.setVolume(this._channelRef.volume);
  }
  stop() {
    if (this._mediaInstance != null) {
      this._mediaInstance.stop();
    }
  }
  setVolume(v) {
    if (this._mediaInstance != null) {
      this._volume = v;
      if (!this._muted) {
        this._mediaInstance.volume = v;
      }
    }
  }
  mute() {
    if (this._mediaInstance != null && !this._muted) {
      this._muted = true;
      this._soundRef.volume = 0;
    }
  }
  unmute() {
    if (this._mediaInstance != null && this._muted) {
      this._muted = false;
      this._soundRef.volume = this._volume;
    }
  }
  pause() {
    if (this._mediaInstance != null) {
      this._curProg = this._mediaInstance.progress;
      this.stop();
    }
  }
  resume() {
    if (this._mediaInstance != null) {
      this._soundRef.paused = false;
      this.play(true);
      this._curProg = 0;
      this._mediaInstance.refreshPaused;
    }
  }
}

class Wrapper {
  async init() {
  }
  async requestConfig() {
    await Loader.load({
      json: [
        { id: "config", url: "./assets/config.json", type: "json" }
      ]
    });
    return Loader.resources["config"].data;
  }
  async requestTicket() {
    await Loader.load({
      images: [
        { id: "ticket", url: "./assets/ticket.json", type: "json" }
      ]
    });
    return Loader.resources["config"].data;
  }
}

class Spine extends Container {
  _paused = false;
  _onAnimationCompletePromiseMap;
  _eventFunctions;
  _spineObject;
  get isPaused() {
    return this._paused;
  }
  get spineObject() {
    return this._spineObject;
  }
  get state() {
    return this._spineObject.state;
  }
  get skeleton() {
    return this._spineObject.skeleton;
  }
  get stateData() {
    return this._spineObject.stateData;
  }
  constructor(id) {
    super(0, 0, typeof id == "string" ? id : id.id);
    this._spineObject = new PIXISpine.Spine(Loader.resources[typeof id == "string" ? id : id.id].spineData);
    this.addChild(this._spineObject);
    this._spineObject.autoUpdate = false;
    this._onAnimationCompletePromiseMap = /* @__PURE__ */ new Map();
    this._eventFunctions = /* @__PURE__ */ new Map();
    this._addSpineListeners();
  }
  clearAllListeners() {
    this._onAnimationCompletePromiseMap.clear();
    this._eventFunctions.clear();
  }
  onAnimationComplete(animName, onCompleteFunc, once = false) {
    const pushedFunction = () => {
      if (once) {
        this._onAnimationCompletePromiseMap.set(animName, () => {
        });
      }
      onCompleteFunc();
    };
    this._onAnimationCompletePromiseMap.set(animName, pushedFunction);
  }
  onEvent(eventName, callback, once = false) {
    const pushedFunction = () => {
      if (once) {
        this._eventFunctions.get(eventName)?.splice(this._eventFunctions.get(eventName).indexOf(pushedFunction), 1);
      }
      callback();
    };
    if (this._eventFunctions.has(eventName)) {
      this._eventFunctions.get(eventName).push(pushedFunction);
    } else {
      this._eventFunctions.set(eventName, [pushedFunction]);
    }
  }
  update = (dT) => {
    this._spineObject.update(dT);
  };
  pause() {
    if (!this._paused) {
      this._paused = true;
    }
  }
  unpause() {
    if (this._paused) {
      this._paused = false;
    }
  }
  destroy() {
    gsap.killTweensOf(this);
    super.destroy();
  }
  _addSpineListeners() {
    this._spineObject.state.addListener({
      complete: (event) => {
        if (this._onAnimationCompletePromiseMap.get(event.animation.name) != null) {
          this._onAnimationCompletePromiseMap.get(event.animation.name)();
        }
      },
      event: (_, a) => {
        if (this._eventFunctions.has(a.data.name)) {
          this._eventFunctions.get(a.data.name)?.forEach((func) => {
            func();
          });
        }
      }
    });
  }
}

class Sprite extends PIXI.Sprite {
  get scaleX() {
    return this.scale.x;
  }
  set scaleX(x) {
    this.scale.x = x;
  }
  get scaleY() {
    return this.scale.y;
  }
  constructor(x, y, spriteID) {
    super(PIXI.Texture.from(spriteID));
    this.position.set(x, y);
    this.anchor.set(0.5, 0.5);
  }
  destroy(options) {
    gsap.killTweensOf(this);
    super.destroy(options == null ? { children: true } : options);
  }
}

var scarf = /*#__PURE__*/Object.freeze({
    __proto__: null,
    Sprite: Sprite,
    Wrapper: Wrapper,
    Sound: Sound,
    Spine: Spine,
    Container: Container
});

class SoundManager {
  _defaultSettings = {
    allowMultiple: false,
    overwrite: true,
    loop: false
  };
  _channels;
  constructor() {
    sound = this;
    this._channels = /* @__PURE__ */ new Map([]);
  }
  init() {
    this._channels.set("default", new SoundChannel("default"));
    this._channels.set("bgm", new SoundChannel("default"));
  }
  async play(id, channel = "default", _settings) {
    const settings = {
      ...this._defaultSettings,
      ..._settings
    };
    if (this._channels.get(channel) == null) {
      this._channels.set(channel, new SoundChannel(channel));
    }
    const soundToPlay = new Sound(id, this._channels.get(channel));
    this._channels.get(channel).addSound(soundToPlay);
    soundToPlay.play(settings.loop);
    return soundToPlay;
  }
  stop() {
  }
  stopAllSoundsByName() {
  }
  getVolume(channel) {
    if (channel != null) {
      if (this._channels.get(channel) != null) {
        return this._channels.get(channel).volume;
      }
    }
    return 0;
  }
  setVolume(volume, channel) {
    if (channel != null) {
      if (this._channels.get(channel) != null) {
        this._channels.get(channel).setVolume(volume);
        return;
      }
    }
  }
  mute(channel) {
    if (channel != null) {
      if (this._channels.get(channel) != null) {
        this._channels.get(channel).mute();
        return;
      }
    }
  }
  unmute(channel) {
    if (channel != null) {
      if (this._channels.get(channel) != null) {
        this._channels.get(channel).unmute();
        return;
      }
    }
  }
  pause(channel) {
    if (channel != null) {
      if (this._channels.get(channel) != null) {
        this._channels.get(channel).pause();
        return;
      }
    }
  }
  resume(channel) {
    if (channel != null) {
      if (this._channels.get(channel) != null) {
        this._channels.get(channel).resume();
        return;
      }
    }
  }
}
let sound;

class ScarfGame {
  static _instance;
  _renderer;
  _stage;
  _settings;
  _wrapper;
  _scale;
  _pause;
  _sound;
  _updatesPaused = false;
  static get instance() {
    return this._instance;
  }
  get stage() {
    return this._stage;
  }
  get wrapper() {
    return this._wrapper;
  }
  get scale() {
    return this._scale;
  }
  get sound() {
    return this._sound;
  }
  get loader() {
    return Loader.resources;
  }
  get settings() {
    return this._settings;
  }
  constructor(settings) {
    ScarfGame._instance = this;
    game = this;
    this._settings = settings;
    this._wrapper = this._settings.wrapper;
    this._renderer = new PIXI.Renderer({
      backgroundColor: 16711680,
      width: window.innerWidth,
      height: window.innerHeight,
      resolution: devicePixelRatio,
      autoDensity: true
    });
    document.body.appendChild(this._renderer.view);
    this._stage = new Container();
    this._scale = new ScaleManager(this._settings);
    this._pause = new PauseManager(this._settings.globalTimeline, (state) => {
      this._updatesPaused = state;
    });
    this._sound = new SoundManager();
    gsap.ticker.add(this._gameLoop);
  }
  async init() {
    window["PIXI"] = PIXI;
    window["__PIXI_INSPECTOR_GLOBAL_HOOK__"] && window["__PIXI_INSPECTOR_GLOBAL_HOOK__"].register({ PIXI });
    await this._wrapper.init();
    const loadingScene = this.stage.addChild(this._settings.loader);
    loadingScene.create();
    Loader.loaderScene = loadingScene;
    await Loader.load(this._settings.loadManifest);
  }
  pauseGame() {
    this._pause.pause();
  }
  resumeGame() {
    this._pause.resume();
  }
  _gameLoop = (_time, _dt, _frame) => {
    this._scale.updateScale(this._renderer, this._stage);
    this._pause.update();
    this._updateGroups(this._stage, _dt);
    this._renderer.render(this._stage);
  };
  _updateGroups(g, dt) {
    if (g["update"] != null) {
      if (!this._updatesPaused) {
        if (g instanceof Container) {
          g.update(dt * 1e-3);
        }
      }
    }
    g.children.forEach((c) => {
      this._updateGroups(c, dt);
    });
  }
}
let game = ScarfGame.instance;

class Scene extends Container {
  async preload(preloadManifest) {
    if (preloadManifest != null) {
      await Loader.load(preloadManifest);
    }
    this.create();
  }
  create() {
  }
}

class DefaultLoadingScene extends Scene {
  _logo = new Text("0%");
  _barBackground = new Graphics();
  _barForeground = new Graphics();
  create() {
    this.displayScaleMode = EDisplayScaleMode.FIT;
    const bg = this.addChild(new Graphics());
    bg.beginFill(13421823, 1);
    bg.drawRect(0, 0, 1334, 750);
    this._logo.position.set(1334 / 2, 300);
    this._logo.anchor.set(0.5, 0.5);
    this.addChild(this._logo);
    this._barBackground.beginFill(4465407, 1);
    this._barBackground.lineStyle(3, 0, 1);
    this._barBackground.drawRect(-500, -12, 1e3, 24);
    this._barBackground.position.set(1334 / 2, 360);
    this.addChild(this._barBackground);
    this._barForeground.beginFill(16777215, 1);
    this._barForeground.lineStyle(3, 0, 1);
    this._barForeground.drawRect(0, -12, 1e3, 24);
    this._barForeground.position.set(1334 / 2 - 500, 360);
    this._barForeground.scale.x = 0;
    this.addChild(this._barForeground);
  }
  setLoadingPercentage(percentage) {
    this._barForeground.scale.x = percentage;
    this._logo.text = Math.floor(percentage * 100) + "%";
  }
}

const defaultGameSettings = {
  loadManifest: {},
  wrapper: new Wrapper(),
  loader: new DefaultLoadingScene(),
  display: {
    nativeWidth: 1334,
    nativeHeight: 750,
    canvasWidth: "dynamic",
    canvasHeight: "dynamic",
    orientation: "landscape"
  },
  globalTimeline: gsap.globalTimeline
};

var index = /*#__PURE__*/Object.freeze({
    __proto__: null,
    ScaleManager: ScaleManager,
    SoundManager: SoundManager
});

const centerX = () => {
  return ScarfGame.instance.settings.display.nativeWidth / 2;
};
const centerY = () => {
  return ScarfGame.instance.settings.display.nativeHeight / 2;
};

class Idle {
  static _idleStorageMap = /* @__PURE__ */ new Map();
  _config;
  _idleStartFunctions = [];
  _idleInterruptFunctions = [];
  constructor(name, delay) {
    this._config = {
      name,
      delay
    };
  }
  static create(name) {
    return false;
  }
  static get(name) {
    const idleToReturn = this._idleStorageMap.get(name);
    if (idleToReturn != null) {
      return idleToReturn;
    } else {
      return;
    }
  }
  static destroy(name) {
    return false;
  }
  static start(name, delayOverride = -1) {
    return false;
  }
  static interrupt(name) {
    return false;
  }
  start() {
    this._idleStartFunctions.forEach((idleFunction) => {
      idleFunction();
    });
  }
  interrupt() {
    this._idleInterruptFunctions.forEach((idleFunction) => {
      idleFunction();
    });
  }
  destroy() {
  }
}

export { EDisplayScaleMode, Idle, index as Managers, scarf as Scarf, ScarfGame, SoundManager, centerX, centerY, defaultGameSettings, game, sound };
//# sourceMappingURL=scarf.js.map
