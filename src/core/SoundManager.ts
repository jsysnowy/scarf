import { Scarf } from '..';
import { SoundChannel } from '../components/SoundChannel';
import { Sound } from '../scarf';

/**
 * Controls playing of sounds in a game:
 *
 * Required features:
 * Play
 * Loop
 * Stop
 * Volume
 * NumberOfSoundInstances ( Stop sound overlapping )
 * Channels
 * BGM/SFX split
 * Pause
 * Mute
 *
 * @export
 * @class SoundManager
 */
export class SoundManager {
    /**
     * Stores the default settings for the sound manager for if bespoke audio settings are not provided:
     * @private
     * @memberof SoundManager
     */
    private _defaultSettings = {
        allowMultiple: false,
        overwrite: true,
        loop: false,
    };

    /**
     * Stores channels
     * @private
     * @type {Scarf.Sound[]}
     * @memberof SoundManager
     */
    private _channels: Map<string, SoundChannel>;

    /**
     * Creates an instance of SoundManager.
     * @memberof SoundManager
     */
    public constructor() {
        // Stores instance into exported "sound" property.
        sound = this;

        // Creates the default channels:
        this._channels = new Map([]);
    }

    /**
     * Initializes the Scarf sound manager:
     * @memberof SoundManager
     */
    public init(): void {
        this._channels.set('default', new SoundChannel('default'));
        this._channels.set('bgm', new SoundChannel('default'));
    }

    /**
     * Plays a track - pass in ID (string) of track you want to play along with channel name.  If passed in channel doesn't exist, it will create it using default settings.
     * Optinal settings param allows extra configuration, falling back to default audio settings.

     * @return {*}  {Promise<void>}
     * @memberof SoundManager
     */
    public async play(
        id: string,
        channel: string = 'default',
        _settings?: {
            allowMultiple?: boolean;
            overwrite?: boolean;
            loop?: boolean;
        }
    ): Promise<Sound> {
        // Build settings object using default settings ontop of anythhing passed in:
        const settings = {
            ...this._defaultSettings,
            ..._settings,
        };

        // Check if the sound channel trying to play in exists, if not need to make it:
        if (this._channels.get(channel) == null) {
            this._channels.set(channel, new SoundChannel(channel));
        }

        // Create new sound instance to play:
        const soundToPlay = new Sound(id, this._channels.get(channel)!);
        this._channels.get(channel)!.addSound(soundToPlay);

        // Sound finally gets told to play:
        soundToPlay.play(settings.loop);

        return soundToPlay;
    }

    /**
     * Tells the soundmanager to stop a certain sound.
     * @memberof SoundManager
     */
    public stop(): void {}

    /**
     * Tells sound manafer ti stop all sounds with a certain name:
     * @memberof SoundManager
     */
    public stopAllSoundsByName(): void {}

    /**
     * Returns volume of soundMnager, or a specific channel.
     * @param {string} [channel]
     * @return {*}  {number}
     * @memberof SoundManager
     */
    public getVolume(channel?: string): number {
        if (channel != null) {
            if (this._channels.get(channel) != null) {
                return this._channels.get(channel)!.volume;
            }
        }
        return 0;
    }

    /**
     * Sets volume of the soundmanager, or a specific channel.
     * @param {number} volume
     * @param {string} [channel]
     * @memberof SoundManager
     */
    public setVolume(volume: number, channel?: string): void {
        if (channel != null) {
            if (this._channels.get(channel) != null) {
                this._channels.get(channel)!.setVolume(volume);
                return;
            }
        }
    }

    /**
     * Mutes all sounds, or a specific channel.
     * @memberof SoundManager
     */
    public mute(channel?: string): void {
        if (channel != null) {
            if (this._channels.get(channel) != null) {
                this._channels.get(channel)!.mute();
                return;
            }
        }
    }

    /**
     * Unmutes all sounds, or a specific channel.
     * @memberof SoundManager
     */
    public unmute(channel?: string): void {
        if (channel != null) {
            if (this._channels.get(channel) != null) {
                this._channels.get(channel)!.unmute();
                return;
            }
        }
    }

    /**
     * Pauses all sounds, or a specific channel.
     * @memberof SoundManager
     */
    public pause(channel?: string): void {
        if (channel != null) {
            if (this._channels.get(channel) != null) {
                this._channels.get(channel)!.pause();
                return;
            }
        }
    }

    /**
     * Unpauses all sounds, or a specific channel.
     * @memberof SoundManager
     */
    public resume(channel?: string): void {
        if (channel != null) {
            if (this._channels.get(channel) != null) {
                this._channels.get(channel)!.resume();

                return;
            }
        }
    }
}

/**
 * Exported instance of soundmanager accessable directly through global scope.
 * */
export let sound: SoundManager;
