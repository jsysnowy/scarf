import * as PIXI from 'pixi.js';
import { IGameSettings } from '../config/DefaultGameSettings';
import { Container } from '../objects/Container';

/**
 * Stores the different possible display scale modes for a container:
 * @export
 * @enum {number}
 */
export enum EDisplayScaleMode {
    NONE,
    FIT,
    FILL,
    STRETCH,
}

export class ScaleManager {
    /**
     * Stores the current available space:
     * @private
     * @type {{ width: number, height: number }}
     * @memberof ScaleManager
     */
    private _currentAvailableSpace: { width: number; height: number } = { width: 1, height: 1 };

    /**
     * Stores the current x, y, min and max ratios of canvas size to native size:
     * @private
     * @type {{ x: number; y: number; min: number; max: number }}
     * @memberof ScaleManager
     */
    private _currentRatio: { x: number; y: number; min: number; max: number } = { x: 1, y: 1, min: 1, max: 1 };

    /**
     * Stores the settings:
     * @private
     * @type {IGameSettings}
     * @memberof ScaleManager
     */
    private _settings: IGameSettings;

    /**
     * Creates an instance of ScaleManager.
     * @param {IGameSettings} settings
     * @memberof ScaleManager
     */
    public constructor(settings: IGameSettings) {
        this._settings = settings;
    }

    /**
     * Updates the renderer dimensions and scales all AspectScaled groups accordingly.
     * @param {PIXI.Renderer} renderer
     * @param {Container} stage
     * @memberof ScaleManager
     */
    public updateScale(renderer: PIXI.Renderer, stage: Container): void {
        // Get the current available space:
        this._currentAvailableSpace = this._getAvailableSpace();

        // Calculate ratios:
        this._currentRatio.x = this._currentAvailableSpace.width / this._settings.display.nativeWidth;
        this._currentRatio.y = this._currentAvailableSpace.height / this._settings.display.nativeHeight;
        this._currentRatio.min = Math.min(this._currentRatio.x, this._currentRatio.y);
        this._currentRatio.max = Math.max(this._currentRatio.x, this._currentRatio.y);

        // Resize the renderer to the available space:
        renderer.resize(this._currentAvailableSpace.width, this._currentAvailableSpace.height);

        // Sort through groups to update scaleModes:
        this._processAspectScale(stage, false);
    }

    /**
     *
     * @private
     * @return {*}  {{ x: number, y: number }}
     * @memberof ScaleManager
     */
    private _getAvailableSpace(): { width: number; height: number } {
        return { width: window.innerWidth, height: window.innerHeight };
    }

    /**
     * Processes aspect scale for a container:
     * @private
     * @memberof ScaleManager
     */
    private _processAspectScale(groupIn: Container, found: boolean): void {
        if (groupIn.displayScaleMode != null && groupIn.displayScaleMode != EDisplayScaleMode.NONE) {
            if (found) {
                console.warn(`[ScarfWarning] Found nested AspectScaledContainer in ${groupIn.name} which was ignored.`);
            } else {
                if (groupIn.displayScaleMode == EDisplayScaleMode.FIT) {
                    found = true;
                    this._applyFitScaleMode(groupIn);
                } else if (groupIn.displayScaleMode == EDisplayScaleMode.FILL) {
                    found = true;
                    this._applyFillScaleMode(groupIn);
                }
            }
        }

        // Recursively check this groups children:
        groupIn.children.forEach((childGroup) => {
            this._processAspectScale(childGroup as Container, found);
        });
    }

    /**
     * Applies a fit scalemode to a container:
     * @private
     * @param {Container} groupIn
     * @memberof ScaleManager
     */
    private _applyFitScaleMode(groupIn: Container): void {
        const topOffset = -((this._settings.display.nativeHeight * this._currentRatio.min - this._currentAvailableSpace.height) * groupIn.scaleAnchor.y);
        const leftOffset = -((this._settings.display.nativeWidth * this._currentRatio.min - this._currentAvailableSpace.width) * groupIn.scaleAnchor.x);
        groupIn.position.set(leftOffset, topOffset);
        groupIn.scale.set(this._currentRatio.min);
    }

    /**
     * Applies a fit scalemode to a container:
     * @private
     * @param {Container} groupIn
     * @memberof ScaleManager
     */
    private _applyFillScaleMode(groupIn: Container): void {
        const topOffset = -((this._settings.display.nativeHeight * this._currentRatio.min - this._currentAvailableSpace.height) * groupIn.scaleAnchor.y);
        const leftOffset = -((this._settings.display.nativeWidth * this._currentRatio.min - this._currentAvailableSpace.width) * groupIn.scaleAnchor.x);
        groupIn.position.set(leftOffset, topOffset);
        groupIn.scale.set(this._currentRatio.min);
    }
}
