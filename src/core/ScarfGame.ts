import { Dict } from '@pixi/utils';
import gsap from 'gsap';
import * as PIXI from 'pixi.js';
import { IGameSettings } from '../config/DefaultGameSettings';
import { DefaultLoadingScene } from '../loader/DefaultLoadingScene';
import { Loader } from '../loader/Loader';
import { Container } from '../objects/Container';
import { Wrapper } from '../scarf';
import { PauseManager } from './PauseManager';
import { ScaleManager } from './ScaleManager';
import { SoundManager } from './SoundManager';
export class ScarfGame {
    /**
     * Stores the latest instance of the ScarfGame as a static variable to be exposed globally:
     * @private
     * @static
     * @type {ScarfGame}
     * @memberof ScarfGame
     */
    private static _instance: ScarfGame;

    /**
     * Stores the main pixi.js renderer:
     * @private
     * @type {Renderer}
     * @memberof ScarfGame
     */
    private _renderer: PIXI.Renderer;

    /**
     * Stores the main game stage - the container which is rendered:
     * @private
     * @type {Container}
     * @memberof ScarfGame
     */
    private _stage: Container;

    /**
     * Stores the settings:
     * @private
     * @type {*}
     * @memberof ScarfGame
     */
    private _settings: IGameSettings;

    /**
     * Stores reference to the wrapper:
     * @private
     * @type {Wrapper}
     * @memberof ScarfGame
     */
    private _wrapper: Wrapper;

    /**
     * Stores a scalemanager instance to handle canvas size:
     * @private
     * @type {ScaleManager}
     * @memberof ScarfGame
     */
    private _scale: ScaleManager;

    /**
     * Stores the PauseManager;
     * @private
     * @type {PauseManager}
     * @memberof ScarfGame
     */
    private _pause: PauseManager;

    /**
     * Stores a soundmanager instance to handle audio:
     * @private
     * @type {SoundManager}
     * @memberof ScarfGame
     */
    private _sound: SoundManager;

    /**
     * Stores if updates are currently paused or not:
     * @private
     * @type {boolean}
     * @memberof ScarfGame
     */
    private _updatesPaused: boolean = false;

    /**
     * Returns the current instance of scarfgame:
     * @readonly
     * @static
     * @type {ScarfGame}
     * @memberof ScarfGame
     */
    public static get instance(): ScarfGame {
        return this._instance;
    }

    /**
     * Returns reference to the main game stage:
     * @readonly
     * @type {Container}
     * @memberof ScarfGame
     */
    public get stage(): Container {
        return this._stage;
    }

    /**
     * Returns instance of the wrapper:
     * @readonly
     * @type {Wrapper}
     * @memberof ScarfGame
     */
    public get wrapper(): Wrapper {
        return this._wrapper;
    }

    /**
     * Returns instance of the scalemanager:
     * @readonly
     * @type {ScaleManager}
     * @memberof ScarfGame
     */
    public get scale(): ScaleManager {
        return this._scale;
    }

    /**
     * Returns instance of the soundmanager:
     * @readonly
     * @type {SoundManager}
     * @memberof ScarfGame
     */
    public get sound(): SoundManager {
        return this._sound;
    }

    /**
     * Returns the loader:
     * @readonly
     * @type {Dict<LoaderResource>}
     * @memberof ScarfGame
     */
    public get loader(): Dict<PIXI.LoaderResource> {
        return Loader.resources;
    }

    /**
     * Returns the game settings:
     * @readonly
     * @type {IGameSettings}
     * @memberof ScarfGame
     */
    public get settings(): IGameSettings {
        return this._settings;
    }

    /**
     * Creates an instance of ScarfGame.
     * @memberof ScarfGame
     */
    public constructor(settings: any) {
        // Store instance:
        ScarfGame._instance = this;
        game = this;

        // Stores the settings:
        this._settings = settings;

        // Store the wrapper:
        this._wrapper = this._settings.wrapper;

        // Creates the renderer:
        this._renderer = new PIXI.Renderer({
            backgroundColor: 0xff0000,
            width: window.innerWidth,
            height: window.innerHeight,
            resolution: devicePixelRatio,
            autoDensity: true,
        });

        // Adds the renderer to the document body:
        document.body.appendChild(this._renderer.view);

        // Add the stage:
        this._stage = new Container();

        // Creates the scale manager:
        this._scale = new ScaleManager(this._settings);
        this._pause = new PauseManager(this._settings.globalTimeline, (state: boolean) => {
            this._updatesPaused = state;
        });

        this._sound = new SoundManager();

        // Game loop:
        gsap.ticker.add(this._gameLoop);
    }

    /**
     * Initializes ScarfGame core:
     * @return {*}  {Promise<void>}
     * @memberof ScarfGame
     */
    public async init(): Promise<void> {
        (window as any)['PIXI'] = PIXI;
        (window as any)['__PIXI_INSPECTOR_GLOBAL_HOOK__'] && (window as any)['__PIXI_INSPECTOR_GLOBAL_HOOK__']!.register({ PIXI: PIXI });

        // Initialise the wrapper:
        await this._wrapper.init();

        const loadingScene = this.stage.addChild(this._settings.loader);
        loadingScene.create();
        Loader.loaderScene = loadingScene as DefaultLoadingScene;

        // Loads assets:
        await Loader.load(this._settings.loadManifest);
    }

    /**
     * Pauses the game:
     * @memberof ScarfGame
     */
    public pauseGame(): void {
        this._pause.pause();
    }

    /**
     * Resumes the game:
     * @memberof ScarfGame
     */
    public resumeGame(): void {
        this._pause.resume();
    }

    /**
     * Main gameloop of ScarfGame:
     * @private
     * @param {number} _time
     * @param {number} _dt
     * @param {number} _frame
     * @memberof ScarfGame
     */
    private _gameLoop = (_time: number, _dt: number, _frame: number): void => {
        // Update canvas scale:
        this._scale.updateScale(this._renderer, this._stage);

        // Update pause manager:
        this._pause.update();

        // Call update on all classes:
        this._updateGroups(this._stage, _dt);

        // Render the stage:
        this._renderer.render(this._stage);
    };

    /**
     * Updates through and "update" funcion if it exists:
     * @private
     * @param {*} g
     * @param {number} dt
     * @memberof ScarfGame
     */
    private _updateGroups(g: any, dt: number): void {
        // Check if this group has an update function:
        if (g['update'] != null) {
            // Check if updates are not currently paused:
            if (!this._updatesPaused) {
                // Check if g is a container:
                if (g instanceof Container) {
                    // Update the group:
                    g.update(dt * 0.001);
                }
            }
        }

        // Update all the groups children:
        g.children.forEach((c: any) => {
            this._updateGroups(c, dt);
        });
    }
}

export let game = ScarfGame.instance;
