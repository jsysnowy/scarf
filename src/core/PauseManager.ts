import gsap from 'gsap';

/**
 * Handles the game pausing/resuming elements:
 * @export
 * @class PauseManager
 */
export class PauseManager {
    /**
     * Stores the main paused flag which halts any game elements if true.
     * @private
     * @type {boolean}
     * @memberof PauseManager
     */
    private _paused: boolean = false;

    /**
     * Stores the set update pause function:
     * @private
     * @type {(state: boolean)}
     * @memberof PauseManager
     */
    private _setUpdatePause: (state: boolean) => void;

    /**
     * Stores the global gsap timeline:
     * @private
     * @type {gsap.core.Timeline}
     * @memberof PauseManager
     */
    private _globalTimeline: gsap.core.Timeline;

    /**
     * Array of all paused tweens:
     * @private
     * @type {gsap.core.Tween[]}
     * @memberof PauseManager
     */
    private _pausedTweens: gsap.core.Tween[] = [];

    /**
     * Stores all paused timelines:
     * @private
     * @type {gsap.core.Timeline[]}
     * @memberof PauseManager
     */
    private _pausedTimelines: gsap.core.Timeline[] = [];

    /**
     * Stores delayed calls:
     * @private
     * @type {gsap.core.Tween[]}
     * @memberof PauseManager
     */
    private _pausedDelayedCalls: gsap.core.Tween[] = [];

    /**
     * Creates an instance of PauseManager.
     * @param {gsap.core.Timeline} timeline
     * @memberof PauseManager
     */
    constructor(timeline: gsap.core.Timeline, setPauseState: (state: boolean) => void) {
        this._globalTimeline = timeline;
        this._setUpdatePause = setPauseState;
    }

    /**
     * Pauses the game:
     * @return {*}  {boolean}
     * @memberof PauseManager
     */
    pause(): boolean {
        this._paused = true;
        return this._paused;
    }

    /**
     * Resumes the game:
     * @return {*}  {boolean}
     * @memberof PauseManager
     */
    resume(): boolean {
        this._paused = false;
        this.resumeAllTweens();
        this.resumeAllUpdates();
        return this._paused;
    }

    /**
     * Updates the pause manager:
     * @memberof PauseManager
     */
    update(): void {
        if (this._paused) {
            this.pauseAllTweens();
            this.pauseAllUpdates();
        }
    }

    /**
     * Pauses all tweens:
     * @memberof PauseManager
     */
    pauseAllTweens(): void {
        // Get all tweens:
        const allTweens = this._globalTimeline.getChildren(false, true, true);

        // Loop over all tweens and pause them if they are not already paused:
        allTweens.forEach((tween) => {
            // Check tweens:
            if (tween instanceof gsap.core.Tween) {
                if (!tween.paused()) {
                    // Pause them,
                    tween.timeScale(0);
                    tween.paused(true);

                    //push them to pausedTweensArr:
                    this._pausedTweens.push(tween);
                }
            } else {
                if (!tween.paused()) {
                    // Pause them,
                    tween.timeScale(0);
                    tween.paused(true);

                    //push them to pausedTweensArr:
                    this._pausedTimelines.push(tween);
                }
            }
        });
    }

    /**
     * All tweens paused via pauseManager are resumed:
     * @memberof PauseManager
     */
    resumeAllTweens(): void {
        this._pausedTweens.forEach((tween) => {
            tween.timeScale(1);
            tween.resume();
        });
        this._pausedTimelines.forEach((tween) => {
            tween.timeScale(1);
            tween.resume();
        });
        this._pausedTweens = [];
        this._pausedTimelines = [];
    }

    /**
     * All game updates get paused.
     * @memberof PauseManager
     */
    pauseAllUpdates(): void {
        this._setUpdatePause(true);
    }

    /**
     * All game updates get resumed.
     * @memberof PauseManager
     */
    resumeAllUpdates(): void {
        this._setUpdatePause(false);
    }

    pauseAllSounds(): void {}
}
