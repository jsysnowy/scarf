import gsap from 'gsap';
import * as PIXI from 'pixi.js';
import { EDisplayScaleMode } from '../core/ScaleManager';

export class Container extends PIXI.Container {
    /**
     * Stores a potential update function which will be called if it exists:
     * @memberof Container
     */
    public update?: (dt: number) => void;

    /**
     * Stores if this container can be paused, or not.  This affects the container, and all children recursively.
     * @type {boolean}
     * @memberof Container
     */
    public canBePaused: boolean = true;

    /**
     * Stores the display scale mode of this container:
     * @private
     * @type {EDisplayScaleMode}
     * @memberof Container
     */
    private _displayScaleMode: EDisplayScaleMode = EDisplayScaleMode.NONE;

    /**
     * Stores the scale anchor:
     * @private
     * @type {{ x: number, y: number }}
     * @memberof Container
     */
    private _scaleAnchor: { x: number; y: number } = { x: 0.5, y: 0.5 };

    /**
     * Sets the display scale mode:
     * @memberof Container
     */
    public set displayScaleMode(v: EDisplayScaleMode) {
        this._displayScaleMode = v;
    }

    /**
     * Returns the scale mode:
     * @type {EDisplayScaleMode}
     * @memberof Container
     * @remark This doesnt work
     */
    public get displayScaleMode(): EDisplayScaleMode {
        return this._displayScaleMode;
    }

    /**
     * Returns the scale anchor:
     * @readonly
     * @type {{ x: number, y: number }}
     * @memberof Container
     */
    public get scaleAnchor(): { x: number; y: number } {
        return this._scaleAnchor;
    }

    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    public get scaleX(): number {
        return this.scale.x;
    }

    /**
     * Sets scaleX of this container.
     * @memberof Container
     */
    public set scaleX(x: number) {
        this.scale.x = x;
    }

    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    public get scaleY(): number {
        return this.scale.y;
    }

    /**
     * Sets scaleY of this container.
     * @memberof Container
     */
    public set scaleY(y: number) {
        this.scale.y = y;
    }

    /**
     * Creates an instance of Container.
     * @param {number} [x]
     * @param {number} [y]
     * @param {string} [name]
     * @memberof Container
     */
    public constructor(x: number = 0, y: number = 0, name?: string) {
        super();

        this.x = x;
        this.y = y;
        this.name = name || '[Unnamed Container]';
    }

    /**
     * Overwrites destroy with custom things:
     * @memberof Container
     */
    public destroy(): void {
        // Kill any tweens on this Container before it's destroyed:
        gsap.killTweensOf(this);

        // Kill this container and all children of it:
        super.destroy({ children: true });
    }
}
