import gsap from 'gsap';
import * as PIXI from 'pixi.js';

export class Sprite extends PIXI.Sprite {
    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    public get scaleX(): number {
        return this.scale.x;
    }

    /**
     * Sets scaleX of this container.
     * @memberof Container
     */
    public set scaleX(x: number) {
        this.scale.x = x;
    }

    /**
     * Returns scale Y of this container.
     * @type {number}
     * @memberof Container
     */
    public get scaleY(): number {
        return this.scale.y;
    }
    /**
     * Creates an instance of Sprite.
     * @param {number} x
     * @param {number} y
     * @param {string} spriteID
     * @memberof Sprite
     */
    constructor(x: number, y: number, spriteID: string) {
        super(PIXI.Texture.from(spriteID));

        this.position.set(x, y);

        this.anchor.set(0.5, 0.5);
    }

    /**
     * Handles this object being destroyed:
     * @param {(boolean | PIXI.IDestroyOptions)} [options]
     * @memberof Sprite
     */
    public destroy(options?: boolean | PIXI.IDestroyOptions): void {
        // Remove tweens on this sprite:
        gsap.killTweensOf(this);

        // Super destroy. Default childen:true
        super.destroy(options == null ? { children: true } : options);
    }
}
