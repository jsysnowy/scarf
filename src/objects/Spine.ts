import gsap from 'gsap';
import * as PIXISpine from 'pixi-spine';
import { Container } from './Container';
import { IAnimationState, IAnimationStateData, IAnimationStateListener, ISkeleton, ISkeletonData } from 'pixi-spine';
import { Loader } from '../loader/Loader';

/**
 * Check out Spine API for more info:
 * @link {https://github.com/pixijs/spine}
 * @export
 * @class Spine
 * @extends {Container}
 */
export class Spine extends Container {
    /**
     * Stores if this spine is paused or not:
     * @private
     * @type {boolean}
     * @memberof Spine
     */
    private _paused: boolean = false;

    /**
     * Stores functions tied to onCompletes of this spine anim.
     * @private
     * @memberof Spine
     */
    private _onAnimationCompletePromiseMap: Map<string, () => void>;

    /**
     * Stores functions tied to onCompletes of this spine anim.
     * @private
     * @type {*}
     * @memberof Spine
     */
    private _eventFunctions: Map<string, [() => void]>;

    /**
     * Stores the spine object:
     * @private
     * @type {( PIXISpine.Spine;}
     * @memberof Spine
     */
    private _spineObject: PIXISpine.Spine;

    /**
     * Returns if this spine is paused or not:
     * @readonly
     * @type {boolean}
     * @memberof Spine
     */
    public get isPaused(): boolean {
        return this._paused;
    }

    /**
     * Return the spine object for this instance of core.Spine.
     * @readonly
     * @type {PIXISpine.Spine}
     * @memberof Spine
     */
    public get spineObject(): PIXISpine.Spine {
        return this._spineObject;
    }

    /**
     * Returns the spine state:
     * @readonly
     * @type {IAnimationState}
     * @memberof Spine
     */
    public get state(): IAnimationState {
        return this._spineObject.state;
    }

    /**
     * Returns the spine state:
     * @readonly
     * @type {ISkeleton}
     * @memberof Spine
     */
    public get skeleton(): ISkeleton {
        return this._spineObject.skeleton;
    }

    /**
     * Returns the state data for this spine:
     * @readonly
     * @type {IAnimationStateData}
     * @memberof Spine
     */
    public get stateData(): IAnimationStateData {
        return this._spineObject.stateData;
    }

    /**
     * Creates an instance of Spine.
     * @param {string} id
     * @memberof Spine
     */
    public constructor(id: string | { id: string; animations: any; skins: any; events: any; replacableSlots: any }) {
        // Generate the PIXI.Container storing this spine anim.
        super(0, 0, typeof id == 'string' ? id : id.id);

        this._spineObject = new PIXISpine.Spine(Loader.resources[typeof id == 'string' ? id : id.id].spineData as ISkeletonData);
        this.addChild(this._spineObject);

        // Make sure this is tied to our gameloop:
        this._spineObject.autoUpdate = false;

        // Add listeners to the spineobject!
        this._onAnimationCompletePromiseMap = new Map<string, () => void>();
        this._eventFunctions = new Map<string, [() => void]>();

        this._addSpineListeners();
    }

    /**
     * Removes all event listeners for this Spine.
     * @memberof Spine
     */
    public clearAllListeners(): void {
        this._onAnimationCompletePromiseMap.clear();
        this._eventFunctions.clear();
    }

    /**
     * Set onAnimationComplete function!
     * @param {string} animName
     * @param {()=>any} onCompleteFunc
     * @param {boolean} once
     * @memberof Spine
     */
    public onAnimationComplete(animName: string, onCompleteFunc: () => void, once: boolean = false): void {
        // Create function with once self removal line:
        const pushedFunction: () => void = (): void => {
            if (once) {
                this._onAnimationCompletePromiseMap.set(animName, () => {});
            }
            onCompleteFunc();
        };

        // Push function:
        this._onAnimationCompletePromiseMap.set(animName, pushedFunction);
    }

    /**
     * Triggers when a certain event calls.
     * @param {string} eventName
     * @param {()=>any} callback
     * @param {boolean} [once=false]
     * @memberof Spine
     */
    public onEvent(eventName: string, callback: () => void, once: boolean = false): void {
        // Create function with once self removal line:
        const pushedFunction: () => void = (): void => {
            if (once) {
                this._eventFunctions.get(eventName)?.splice(this._eventFunctions.get(eventName)!.indexOf(pushedFunction), 1);
            }
            callback();
        };

        // Push function:
        if (this._eventFunctions.has(eventName)) {
            this._eventFunctions.get(eventName)!.push(pushedFunction);
        } else {
            this._eventFunctions.set(eventName, [pushedFunction]);
        }
    }

    /**
     * Called every frame, this makes sure our spine object is tied into the core game loop.
     * @private
     * @param {number} dT difference in time
     * @memberof Spine
     */
    public update = (dT: number): void => {
        // Update based on deltaTime:
        this._spineObject.update(dT);
    };

    /**
     * Pauses this spine:
     * @memberof Spine
     */
    public pause(): void {
        if (!this._paused) {
            this._paused = true;
        }
    }

    /**
     * Unpauses this spine:
     * @memberof Spine
     */
    public unpause(): void {
        if (this._paused) {
            this._paused = false;
        }
    }

    /**
     * Super also removes the updateFunc.
     * @memberof Spine
     */
    public destroy(): void {
        // Kill any tweens attached to this:
        gsap.killTweensOf(this);

        // Destroy this + all its children:
        super.destroy();
    }

    /**
     * Attaches spine listeners onto this spineObject.
     * @private
     * @memberof Spine
     */
    private _addSpineListeners(): void {
        // Listeners:
        this._spineObject.state.addListener({
            // Event called when animations are completed:
            complete: (event: any) => {
                if (this._onAnimationCompletePromiseMap.get(event.animation.name) != null) {
                    this._onAnimationCompletePromiseMap.get(event.animation.name)!();
                }
            },
            event: (_, a) => {
                if (this._eventFunctions.has(a.data.name)) {
                    this._eventFunctions.get(a.data.name)?.forEach((func) => {
                        func();
                    });
                }
            },
        } as IAnimationStateListener);
    }
}
