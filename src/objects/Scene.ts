import { Loader } from '../loader/Loader';
import { ILoadManifest } from '../loader/LoadManifest';
import { Container } from './Container';

export class Scene extends Container {
    /**
     * Preloads assets needed for the scene:
     * @return {*}  {Promise<void>}
     * @memberof Scene
     */
    async preload(preloadManifest?: ILoadManifest): Promise<void> {
        if (preloadManifest != null) {
            await Loader.load(preloadManifest);
        }

        this.create();
    }

    /**
     * Creates the scene:
     * @memberof Scene
     */
    create(): void {}
}
