import { Sound } from './Sound';

/**
 * Stores an array of sounds which are grouped and controlled together.
 * @export
 * @class SoundChannel
 */
export class SoundChannel {
    /**
     * Stores the default settings for the soundchannel for if bespoke audio settings are not provided:
     * @private
     * @memberof SoundManager
     */
    private _defaultSettings = {
        allowMultiple: false,
        overwrite: true,
        loop: false,
    };

    /**
     * Stores the name of this channel:
     * @private
     * @type {string}
     * @memberof SoundChannel
     */
    private _name: string;

    /**
     * Stores the volume of this channel:
     * @private
     * @type {number}
     * @memberof SoundChannel
     */
    private _volume: number = 1;

    /**
     * Stores if this channel is muted or not:
     * @private
     * @type {boolean}
     * @memberof SoundChannel
     */
    private _muted: boolean = false;

    /**
     * Stores if the channel is paused or not:
     * @private
     * @type {boolean}
     * @memberof SoundChannel
     */
    private _paused: boolean = false;

    /**
     * Stores all the sounds in this channel:
     * @private
     * @type {Sound[]}
     * @memberof SoundChannel
     */
    private _sounds: Sound[] = [];

    /**
     * Returns volume of this channel:
     * @readonly
     * @type {number}
     * @memberof SoundChannel
     */
    public get volume(): number {
        return this._volume;
    }

    /**
     * Creates an instance of SoundChannel.
     * @memberof SoundChannel
     */
    public constructor(name: string) {
        this._name = name;
    }

    /**
     * Adds a sound to this channel:
     * @param {Sound} s
     * @memberof SoundChannel
     */
    public addSound(s: Sound): void {
        if (s.channel != null) {
            s.channel.removeSound(s);
        }
        this._sounds.push(s);
    }

    /**
     * Remove a sound from this channel:
     * @param {Sound} s
     * @memberof SoundChannel
     */
    public removeSound(s: Sound) {
        if (this._sounds.includes(s)) {
            this._sounds.splice(this._sounds.indexOf(s), 1);
        }
    }

    /**
     * Sets volume of this channel:
     * @param {number} v
     * @memberof SoundChannel
     */
    public setVolume(v: number): void {
        this._volume = Math.min(Math.max(v, 0), 1);
        this._sounds.forEach((sound) => {
            sound.setVolume(this._volume);
        });
    }

    /**
     * Mute this channel:
     * @memberof SoundChannel
     */
    public mute(): void {
        this._muted = true;
        this._sounds.forEach((sound) => {
            sound.mute();
        });
    }

    /**
     * Unmute this channel:
     * @memberof SoundChannel
     */
    public unmute(): void {
        this._muted = false;
        this._sounds.forEach((sound) => {
            sound.unmute();
        });
    }

    /**
     * Pause this channel:
     * @memberof SoundChannel
     */
    public pause(): void {
        this._paused = true;
        this._sounds.forEach((sound) => {
            sound.pause();
        });
    }

    /**
     * Resume this channel:
     * @memberof SoundChannel
     */
    public resume(): void {
        this._paused = false;
        this._sounds.forEach((sound) => {
            sound.resume();
        });
    }
}
