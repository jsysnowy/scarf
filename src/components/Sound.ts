import '@pixi/sound';
import { Sound as PIXISound, IMediaInstance } from '@pixi/sound';
import { Loader } from '../loader/Loader';
import { SoundChannel } from './SoundChannel';

export class Sound {
    /**
     * Stores the audiosprite ID - if this sound is audiosprite instance:
     * @private
     * @type {string}
     * @memberof Sound
     */
    private _audioSpriteID?: string;

    /**
     * Stores the soundID of this sound:
     * @private
     * @type {string}
     * @memberof Sound
     */
    private _soundID: string;

    /**
     * Stores the channel reference:
     * @private
     * @type {SoundChannel}
     * @memberof Sound
     */
    private _channelRef: SoundChannel;

    /**
     * Stores the pixi sound.
     * @private
     * @type {PIXISound}
     * @memberof Sound
     */
    private _soundRef?: PIXISound;

    /**
     * Stores the media instance:
     * @private
     * @type {IMediaInstance}
     * @memberof Sound
     */
    private _mediaInstance?: IMediaInstance;

    /**
     * Stores the sounds volume:
     * @private
     * @type {number}
     * @memberof Sound
     */
    private _volume: number = 1;

    /**
     * Stores if the sound is muted or not:
     * @private
     * @type {boolean}
     * @memberof Sound
     */
    private _muted: boolean = false;

    /**
     * Stores if the sound is paused or not:
     * @private
     * @type {boolean}
     * @memberof Sound
     */
    private _paused: boolean = false;

    /**
     * Stores current progress:
     * @private
     * @type {number}
     * @memberof Sound
     */
    private _curProg: number = 0;

    /**
     * Stores duration of the sound:
     * @private
     * @type {number}
     * @memberof Sound
     */
    private _soundDuration: number = 0;

    /**
     * Returns the soundID of this sound:
     * @readonly
     * @type {string}
     * @memberof Sound
     */
    public get soundID(): string {
        return this._soundID;
    }

    /**
     * Returns the media instance:
     * @readonly
     * @type {IMediaInstance}
     * @memberof Sound
     */
    public get mediaInstance(): IMediaInstance {
        return this._mediaInstance as IMediaInstance;
    }

    /**
     * Sets the channel this sound is playing in:
     * @memberof Sound
     */
    public set channel(c: SoundChannel) {
        this._channelRef = c;
    }

    /**
     * Gets the sound channel this sound is playing in
     * @type {SoundChannel}
     * @memberof Sound
     */
    public get channel(): SoundChannel {
        return this._channelRef;
    }

    /**
     * Creates an instance of Sound.
     * @param {string} id
     * @memberof Sound
     */
    public constructor(id: string, channel: SoundChannel) {
        if (id.includes('/')) {
            this._audioSpriteID = id.split('/')[0];
            this._soundID = id.split('/')[1];
        } else {
            this._soundID = id;
        }

        this._channelRef = channel;
    }

    /**
     * Tells this sound instance to play:
     * @param {boolean} _loop
     * @return {*}  {Promise<void>}
     * @memberof Sound
     */
    public async play(_loop: boolean): Promise<void> {
        // Creates the sound and plays the sound:
        this._soundRef = Loader.resources[this._audioSpriteID != null ? this._audioSpriteID : this._soundID].sound!;
        if (this._audioSpriteID == null) {
            this._soundDuration = this._soundRef.duration;
        } else {
            this._soundDuration = this._soundRef.sprites[this._soundID].duration;
        }

        // Gets the media instance, sets the loop property and calls play on it:
        if (this._soundRef != null) {
            this._soundRef.loop = _loop;
            const instance =
                this._audioSpriteID != null
                    ? this._soundRef.play({ sprite: this._soundID, start: this._curProg * this._soundDuration })
                    : this._soundRef.play({ start: this._curProg * this._soundDuration });

            if (instance instanceof Promise) {
                this._mediaInstance = await instance;
            } else {
                this._mediaInstance = instance as IMediaInstance;
            }
        }

        // Apply properties from the channel onto this sound:
        this.setVolume(this._channelRef.volume);
    }

    /**
     * Tells this sound instance to stop:
     * @memberof Sound
     */
    public stop(): void {
        if (this._mediaInstance != null) {
            this._mediaInstance.stop();
        }
    }

    /**
     * Sets volume of this media instance:
     * @param {number} v
     * @memberof Sound
     */
    public setVolume(v: number): void {
        if (this._mediaInstance != null) {
            this._volume = v;
            if (!this._muted) {
                this._mediaInstance.volume = v;
            }
        }
    }

    /**
     * Mutes this mediaInstance:
     * @memberof Sound
     */
    public mute(): void {
        if (this._mediaInstance != null && !this._muted) {
            this._muted = true;
            this._soundRef!.volume = 0;
        }
    }

    /**
     * Unmutes this media instance:
     * @memberof Sound
     */
    public unmute(): void {
        if (this._mediaInstance != null && this._muted) {
            this._muted = false;
            this._soundRef!.volume = this._volume;
        }
    }

    /**
     * Pauses this sound:
     * @memberof Sound
     */
    public pause(): void {
        if (this._mediaInstance != null) {
            this._curProg = this._mediaInstance.progress;
            this.stop();
        }
    }

    /**
     * Resumes this sound:
     * @memberof Sound
     */
    public resume(): void {
        if (this._mediaInstance != null) {
            this._soundRef!.paused = false;
            this.play(true);
            this._curProg = 0;
            this._mediaInstance.refreshPaused;
        }
    }
}
