import { ScarfGame } from '../core/ScarfGame';

/**
 * Returns half of the native width of the game, useful for positioning items on screen.
 * @returns number
 */
export const centerX = (): number => {
    return ScarfGame.instance.settings.display.nativeWidth / 2;
};

/**
 * Returns half of the native height of the game, useful for positioning items on screen.
 * @returns number
 */
export const centerY = (): number => {
    return ScarfGame.instance.settings.display.nativeHeight / 2;
};
