// Controls CTA / Idles / Timeouts in game using GSAP timelines and tweens.

export interface IIdle {
    delay: number;
    name: string;
}

export class Idle {
    /**
     * Stores all creates idles
     * @private
     * @static
     * @type {Map<string, Idle>}
     * @memberof Idle
     */
    private static _idleStorageMap: Map<string, Idle> = new Map<string, Idle>();

    /**
     * Stores this idles config:
     * @private
     * @type {IIdle}
     * @memberof Idle
     */
    private _config: IIdle;

    /**
     * Stores an array of all idleFunctions handling idles starting to trigger on delay resolve.
     * @private
     * @type {((...params: any[]) => void)[]}
     * @memberof Idle
     */
    private _idleStartFunctions: ((...params: any[]) => void)[] = [];

    /**
     * Stores an array of all idleFunctions handling idles starting interrupting.
     * @private
     * @type {((...params: any[]) => void)[]}
     * @memberof Idle
     */
    private _idleInterruptFunctions: ((...params: any[]) => void)[] = [];

    /**
     * Creates an instance of Idle.
     * @param {string} name
     * @param {number} delay
     * @memberof Idle
     */
    private constructor(name: string, delay: number) {
        this._config = {
            name,
            delay,
        };
    }

    /**
     * Creates a new idle using the factory. Returns false if the idle already existed.
     * @static
     * @param {string} name
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static create(name: string): boolean {
        return false;
    }

    /**
     * Gets an idle from the idleStorageMap based on passed in name.
     * @static
     * @param {string} name
     * @return {*}  {Idle}
     * @memberof Idle
     */
    static get(name: string): Idle | undefined {
        const idleToReturn = this._idleStorageMap.get(name);
        if (idleToReturn != null) {
            return idleToReturn;
        } else {
            return;
        }
    }

    /**
     * Destroys idle with passed in name if it exists. Returns false if the idle didn't exist.
     * @static
     * @param {string} name
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static destroy(name: string): boolean {
        return false;
    }

    /**
     * Starts idle with the passed in name if it exists. Returns false if the idle didn't exist.  Optional delay override for changing the delay on this start.
     * @static
     * @param {string} name
     * @param {number} [delayOverride=-1]
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static start(name: string, delayOverride: number = -1): boolean {
        return false;
    }

    /**
     * Interrupts idle with the passed in name if it exists. Returns false if the idle didn't exist.
     * @static
     * @param {string} name
     * @return {*}  {boolean}
     * @memberof Idle
     */
    static interrupt(name: string): boolean {
        return false;
    }

    /**
     * All idle start functions call.
     * @memberof Idle
     */
    start(): void {
        this._idleStartFunctions.forEach((idleFunction) => {
            idleFunction();
        });
    }

    /**
     * All idle interrupt functions call.
     * @memberof Idle
     */
    interrupt(): void {
        this._idleInterruptFunctions.forEach((idleFunction) => {
            idleFunction();
        });
    }

    /**
     * Destroys this idle. Cleanup listeners.
     * @memberof Idle
     */
    destroy(): void {}
}
