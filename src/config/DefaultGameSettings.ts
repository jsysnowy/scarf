import gsap from 'gsap';
import { DefaultLoadingScene } from '../loader/DefaultLoadingScene';
import { ILoadManifest } from '../loader/LoadManifest';
import { Scene } from '../objects/Scene';
import { Wrapper } from '../scarf';

export interface IGameDisplaySettings {
    nativeWidth: number;
    nativeHeight: number;
    canvasWidth: number | 'dynamic';
    canvasHeight: number | 'dynamic';
    orientation: 'portrait' | 'landscape' | 'dynamic';
}

export interface IGameSettings {
    loadManifest: ILoadManifest;
    loader: Scene;
    wrapper: Wrapper;
    display: IGameDisplaySettings;
    globalTimeline: gsap.core.Timeline;
}

export const defaultGameSettings: IGameSettings = {
    loadManifest: {},
    wrapper: new Wrapper(),
    loader: new DefaultLoadingScene(),
    display: {
        nativeWidth: 1334,
        nativeHeight: 750,
        canvasWidth: 'dynamic',
        canvasHeight: 'dynamic',
        orientation: 'landscape',
    },
    globalTimeline: gsap.globalTimeline,
};
