import * as PIXI from 'pixi.js';
import { Scene } from '../objects/Scene';
import { DefaultLoadingScene } from './DefaultLoadingScene';
import { GenerateSpineAtlas } from './GenerateSpineAtlas';
import { ILoadManifest } from './LoadManifest';

/**
 * @author Rowan
 * @copyright Rowan 2022
 * @license ROW_LIC
 * @export
 * @class Loader
 */
export class Loader {
    /**
     * Stores the loader instance:
     * @private
     * @type {PIXI.Loader}
     * @memberof Loader
     */
    private static _loaderInstance: PIXI.Loader;

    /**
     * Stores the loader scene:
     * @private
     * @static
     * @type {DefaultLoadingScene}
     * @memberof Loader
     */
    private static _loaderScene: DefaultLoadingScene;

    /**
     * Returns pixi loader resources:
     * @readonly
     * @static
     * @type {PIXI.utils.Dict<PIXI.LoaderResource>}
     * @memberof Loader
     */
    public static get resources(): PIXI.utils.Dict<PIXI.LoaderResource> {
        return this._loaderInstance.resources;
    }

    /**
     * Sets the loaderScene:
     * @static
     * @memberof Loader
     */
    public static set loaderScene(s: DefaultLoadingScene) {
        this._loaderScene = s;
    }

    /**
     * Returns the current loaderScene:
     * @static
     * @type {DefaultLoadingScene}
     * @memberof Loader
     */
    public static get loaderScene(): DefaultLoadingScene {
        return this._loaderScene;
    }

    /**
     * Loads passed in manifest:
     * @param {{ id: string; url: string; type: string }[]} manifest
     * @return {*}  {Promise<void>}
     * @memberof Loader
     */
    public static async load(manifest: ILoadManifest): Promise<void> {
        // Adds all manifest elements to loader:
        if (this._loaderInstance == null) {
            this._loaderInstance = new PIXI.Loader();
        }

        // Get total length of items to load for progress bar:
        let total = manifest.assetCount != null ? manifest.assetCount : 0;
        let c = 0;
        this._loaderInstance.onProgress.add((_l, _r) => {
            c++;
            const progress = c / total;
            if (this._loaderScene != null) {
                this._loaderScene.setLoadingPercentage(progress);
            }
        });

        // Loads json:
        if (manifest.json != null) {
            manifest.json.forEach((element) => {
                if (element.type == 'spritesheet') {
                    // Load in just .json of a spritesheet, the images get pulled in automatically:
                    this._loaderInstance.add(element.id, element.url);
                } else if (element.type == 'spine') {
                    // Spines are ignored and loaded in the second run of the loader:
                } else {
                    // Load the generic json:
                    this._loaderInstance.add(element.id, element.url);
                }
            });
        }

        // Loads images:
        if (manifest.images != null) {
            manifest.images.forEach((element) => {
                this._loaderInstance.add(element.id, element.url);
            });
        }

        // Loads audio:
        if (manifest.audio != null) {
            manifest.audio.forEach((element) => {
                this._loaderInstance.add(element.id, element.url);
            });
        }

        // Loads audiosprite audio files:
        if (manifest.audiosprites != null) {
            manifest.audiosprites.forEach((element) => {
                this._loaderInstance.add(element.id, element.url);
            });
        }

        if (manifest.warnings != null) {
            if (manifest.warnings != null && manifest.warnings.length != 0) {
                console.group('Warnings');
                manifest.warnings.forEach((err) => {
                    console.warn(`[Scarf Warning] Missing image: ${err[1]} from spine ${err[3]}`);
                });
                console.groupEnd();
            }
        }

        // Load 1: Gets: Images/Audio/Json.
        await new Promise<void>((resolve, reject) => {
            this._loaderInstance.onError.once(() => {
                reject('Primary load has failed.  This load handled images/spritesheets/json/audio/audiosprite.');
            });

            // Resolve once complete:
            this._loaderInstance.onComplete.once(() => {
                // Load in spines:
                if (manifest.spines != null) {
                    manifest.spines.forEach((element) => {
                        if (element.type == 'atlas') {
                            // Add all spine data:
                            const atlas = GenerateSpineAtlas(element.textures);
                            this._loaderInstance.add(element.id, element.jsonPath, { metadata: { spineAtlas: atlas } });
                        }
                    });
                }

                resolve();
            });

            // Triggers loader to load:
            this._loaderInstance.load();
        });

        // Resets loader for second load:
        this._loaderInstance.onError.detachAll();
        this._loaderInstance.onComplete.detachAll();

        // Load 2: Gets: Spine files.
        await new Promise<void>((resolve, reject) => {
            this._loaderInstance.onError.once(() => {
                reject('Secondary load has failed. This load handled spines.');
            });

            this._loaderInstance.onComplete.once(() => {
                // Adds audiosprite data:
                manifest.audiosprites?.forEach((audiosprite) => {
                    // Get json data for this audiosprite:
                    const audiospriteJsonData = Loader.resources[audiosprite.id + '_data']?.data?.sprite;
                    if (audiospriteJsonData != null) {
                        const audioSpritesOutputData: any = {};

                        Object.keys(audiospriteJsonData).forEach((key) => {
                            audioSpritesOutputData[key] = {
                                start: +audiospriteJsonData[key][0] / 1000,
                                end: +(audiospriteJsonData[key][0] + audiospriteJsonData[key][1]) / 1000,
                            };
                        });
                        Loader.resources[audiosprite.id].sound?.addSprites(audioSpritesOutputData);
                    }
                });

                // Resolves:
                resolve();
            });

            // Starts load 2:
            this._loaderInstance.load();
        });
    }
}
