export interface ILoadManifest {
    images?: [{ id: string; url: string; type: string }];
    json?: [{ id: string; url: string; type: string }];
    audio?: [{ id: string; url: string; type: string }];
    audiosprites?: [{ id: string; url: string; type: string }];
    spines?: [{ id: string; url: string; type: string; textures: string[]; jsonPath: string }];
    warnings?: any[];
    assetCount?: number;
}
