import { DisplayObject, Graphics, Text } from 'pixi.js';
import { EDisplayScaleMode } from '../core/ScaleManager';
import { Scene } from '../objects/Scene';

export class DefaultLoadingScene extends Scene {
    /**
     * Loading logo:
     * @protected
     * @type {Text}
     * @memberof DefaultLoadingScene
     */
    protected _logo: Text = new Text('0%');

    /**
     * Bar background:
     * @protected
     * @type {Graphics}
     * @memberof DefaultLoadingScene
     */
    protected _barBackground: Graphics = new Graphics();

    /**
     * Bar foreground:
     * @protected
     * @type {Graphics}
     * @memberof DefaultLoadingScene
     */
    protected _barForeground: Graphics = new Graphics();

    /**
     * Creates the scene:
     * @memberof DefaultLoadingScene
     */
    create(): void {
        this.displayScaleMode = EDisplayScaleMode.FIT;

        const bg = this.addChild(new Graphics());
        bg.beginFill(0xccccff, 1);
        bg.drawRect(0, 0, 1334, 750);

        this._logo.position.set(1334 / 2, 300);
        this._logo.anchor.set(0.5, 0.5);
        this.addChild(this._logo);

        this._barBackground.beginFill(0x4422ff, 1);
        this._barBackground.lineStyle(3, 0x000000, 1);
        this._barBackground.drawRect(-500, -12, 1000, 24);
        this._barBackground.position.set(1334 / 2, 360);
        this.addChild(this._barBackground);

        this._barForeground.beginFill(0xffffff, 1);
        this._barForeground.lineStyle(3, 0x000000, 1);
        this._barForeground.drawRect(0, -12, 1000, 24);
        this._barForeground.position.set(1334 / 2 - 500, 360);
        this._barForeground.scale.x = 0;
        this.addChild(this._barForeground);
    }

    /**
     * Sets the loader to a new loading percentage:
     * @memberof DefaultLoadingScene
     */
    setLoadingPercentage(percentage: number): void {
        this._barForeground.scale.x = percentage;
        this._logo.text = Math.floor(percentage * 100) + '%';
    }
}
