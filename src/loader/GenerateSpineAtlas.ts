import { TextureAtlas } from 'pixi-spine';
import { Texture } from 'pixi.js';

export const GenerateSpineAtlas = (atlasData: string[]): TextureAtlas => {
    // Generate textureAtlas:
    const allTex: any = {};

    // Gathers all textures:
    atlasData.forEach((texture) => {
        allTex[texture] = Texture.from(texture);
    });

    // Store generates textureAtlas:
    let spineAtlas: TextureAtlas = new TextureAtlas();

    // Attach these textures to our spineAtlas created earlier:
    spineAtlas.addTextureHash(allTex, true);

    const atlas = new TextureAtlas();
    atlas.addTextureHash(allTex, true);
    return atlas;
};
