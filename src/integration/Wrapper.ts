import { Loader } from '../loader/Loader';

export class Wrapper {
    /**
     * Initializes the wrapper:
     * @return {*}  {Promise<void>}
     * @memberof Wrapper
     */
    public async init(): Promise<void> {}

    /**
     * Requests config file from integration:
     * @return {*}  {Promise<void>}
     * @memberof Wrapper
     */
    public async requestConfig(): Promise<{}> {
        // Loads the config.json
        await Loader.load({
            json: [
                { id: 'config', url: './assets/config.json', type: 'json' },
            ],
        });

        // Returns the config data object:
        return Loader.resources['config'].data;
    }

    /**
     * Requests ticket file from integration:
     * @return {*}  {Promise<void>}
     * @memberof Wrapper
     */
    public async requestTicket(): Promise<{}> {
        // Loads the config.json
        await Loader.load({
            images: [
                { id: 'ticket', url: './assets/ticket.json', type: 'json' },
            ],
        });

        // Returns the config data object:
        return Loader.resources['config'].data;
    }
}
